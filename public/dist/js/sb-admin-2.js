/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
$(function() {
    $('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent();

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
});

$(function() {

   $("#formpost").submit(function(e) {
        e.preventDefault();
               
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: "/gallery/post",
            type: "POST",
            data: formData,
            success: function (msg) {
                 if(msg==1){
                    $("#myModal").modal('hide');
                    ajaxlist();
                 }else{
                    alert("form upload has been failed.");
                 }
            },
            contentType: false,
            processData: false
        });

    });

   $("#formedit").submit(function(e) {
        e.preventDefault();
               
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: "/gallery/edit",
            type: "POST",
            data: formData,
            success: function (msg) {
                 if(msg==1){
                    $("#myEdit").modal('hide');
                    ajaxlist();
                 }else{
                    alert("form update has been failed.");
                 }
            },
            contentType: false,
            processData: false
        });

    });

   $('#editbtn').click( function(e){
            e.preventDefault();
            var gallId = $('#editbtn').data("gallid");

            $("#myInfo").modal('hide');
            $("#myEdit").modal();
            $('#txtFile2').val('');
        
        $.get( '/gallery/view/'+gallId, function( data ) {

            $('#gallId').val(gallId);
            $('#imgeditPlace').attr('src', '/public/uploads/'+data.filename );
            $('#txtTitle').val(data.title);

        });

   });

     
});

function ajaxlist(){

         $.getJSON('/gallery/ajaxlist', { id: 0 }, function(data) {
                    $('#ImgGallery').html('');
                    var i = 1;
                    $.each(data, function(index, element) {
                            $('#ImgGallery').append('<tr id="rec'+element.id+'"><td>'+i+'</td><td>'+element.title+'</td><td><img width="50" src="/public/uploads/'+element.filename+'" /></td><td>'+element.filename+'</td><td>'+element.created+'</td><td><button type="button" class="btn btn-warning btn-circle" title="Delete selected record" onclick="record_delete('+element.id+')" ><i class="fa fa-times"></i></button> <button type="button" class="btn btn-info btn-circle" title="View data" onclick="record_view('+element.id+')" ><i class="fa fa-check"></i></button></td></tr>');
                            i++;
                    });
                });   

}


function record_delete(galleryId){


    var r = confirm("Are you sure to delete selected record ? ");
    if (r == true) {

        $.get( '/gallery/delete/'+galleryId, function( data ) {
                ajaxlist();
        });

    } 

}

function record_view(galleryId){

        $.get( '/gallery/view/'+galleryId, function( data ) {

                $("#myInfo").modal();

                //var obj = $.parseJSON(data);
                $('#imgplace').attr('src', '/public/uploads/'+data.filename );
                $('#caption').html(data.title);
                $('#created_date').html(data.created);
                $('#editbtn').data("gallid" ,data.id);

        });

} 




