<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Main extends Controller {

	public function action_index()
	{
		
		$view = View::factory('common/layout');

		$view->title = "Dashboard";
        $view->body = View::factory('dashboard/main');

        $dashboard = $view->render();

        $this->response->body($dashboard);
	}

} 
