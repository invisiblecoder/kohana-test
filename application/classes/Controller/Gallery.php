<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Gallery extends Controller {


	public function action_index()
	{
		
		$galleries = ORM::factory('Gallery')->find_all(); // get all galleries data

		$view = View::factory('common/layout');
		
		$view->title = "Images Gallery"; 
        $view->body = View::factory('gallery/list');
        $view->body->pubgalleries = $galleries; // set galleries value to gallery page view variables

        $gallery_list = $view->render();
        $this->response->body($gallery_list);

	}

	public function action_post()
	{
        
        $title = $_POST['txtTitle'];
        $error_message = NULL;
        $filename = NULL;
 
        if ($this->request->method() == Request::POST)
        {
            if (isset($_FILES['txtFile']['name']))
            {
                if($_FILES['txtFile']['name']){
                   $filename = $this->_save_image($_FILES['txtFile']);
                }   
            }
        }


        $gallery = ORM::factory('Gallery');
        $gallery->title = $title;
   		$gallery->filename = $filename;
		$gallery->created = date("Y-m-d H:i:s");
		$gallery->created_by = 1;

		$gallery->save();

		echo 1;

	}

	public function action_edit()
	{

        $title = $_POST['txtTitle'];
        $gallId = $_POST['gallId'];
        $filename = NULL;
 		//print_r($_FILES);exit();
 		$gallery = ORM::factory('Gallery', $gallId);

        if ($this->request->method() == Request::POST)
        {
            if (isset($_FILES['txtFile2']['name']))
            {
                if($_FILES['txtFile2']['name']){
                  $filename = $this->_save_image($_FILES['txtFile2']);
                  $gallery->filename = $filename;
                }  
            }
        }
        
        $gallery->title = $title;
		$gallery->save();

		echo 1;

	}

	protected function _save_image($image)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'public/uploads/';
 
        if ($file = Upload::save($image, NULL, $directory))
        {
            $filename = strtolower(Text::random('alnum', 12)).'.'.pathinfo($image['name'], PATHINFO_EXTENSION);
 
  
            Image::factory($file)
                ->resize(200, 200, Image::AUTO)
                ->save($directory.$filename);    
 
            // Delete the temporary file
            unlink($file);
 
            return $filename;
        }
 
        return FALSE;
    }

	public function action_delete()
	{ 
	     $gallery_id = $this->request->param('id');

         $galleries = ORM::factory('Gallery', $gallery_id);
         $galleries->delete();
	}

	public function action_ajaxlist(){

		   $galleries = ORM::factory('Gallery')->find_all(); // get all galleries data

		   $arr_gallery = array();

		   foreach ($galleries as $key => $gallery) {
		   	   $arr_gallery[$key]['id'] = $gallery->gallery_id;
		   	   $arr_gallery[$key]['title'] = $gallery->title;
		   	   $arr_gallery[$key]['filename'] = $gallery->filename;
		   	   $arr_gallery[$key]['created'] = date("m/D/Y H:i", strtotime($gallery->created));

		   }

		   $this->response->headers('Content-Type', 'application/json; charset=utf-8');
		   $this->response->body(json_encode($arr_gallery));

	}

	public function action_view(){

		   $gallery_id = $this->request->param('id');
		   			
		   $gallery = ORM::factory('Gallery')->where('gallery_id', '=', $gallery_id)->find(); // get selected data
		   $arr_gallery = array();

		   	   $arr_gallery['id'] = $gallery->gallery_id;
		   	   $arr_gallery['title'] = $gallery->title;
		   	   $arr_gallery['filename'] = $gallery->filename;
		   	   $arr_gallery['created'] = date("m/D/Y H:i", strtotime($gallery->created));


		   $this->response->headers('Content-Type', 'application/json; charset=utf-8');
		   $this->response->body(json_encode($arr_gallery));

	}

} 
