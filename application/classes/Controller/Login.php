<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Login extends Controller {

	public function action_index()
	{
		
		$view = View::factory('common/login');

		// Render the view
        $login = $view->render();
 
        $this->response->body($login);
	}

} // End Welcome
