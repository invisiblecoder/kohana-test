<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Images Gallery</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
<div class="row">
                <!-- /.col-lg-6 -->
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table width="100%" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th width="10%">#</th>
                                            <th width="30%">Title</th>
                                            <th width="15%">Thumbnail</th>
                                            <th>Filename</th>
                                            <th width="15%">Created date</th>
                                            <th width="82">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody id="ImgGallery" class="tooltipshow" >
                                    <?php $i=1;foreach($pubgalleries as $gallery){ ?>
                                        <tr id="rec<?php echo $gallery->gallery_id;?>">
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $gallery->title;?></td>
                                            <td><img width="50" src="/public/uploads/<?php echo $gallery->filename; ?>" /></td>
                                            <td><?php echo $gallery->filename; ?></td>
                                            <td><?php echo date("m/D/Y H:i", strtotime($gallery->created)); ?></td>
                                            <td><button type="button" class="btn btn-warning btn-circle" title="Delete selected record" onclick="record_delete(<?php echo $gallery->gallery_id;?>)" ><i class="fa fa-times"></i>
                            </button> <button type="button" class="btn btn-info btn-circle" title="View data" onclick="record_view(<?php echo $gallery->gallery_id;?>)" ><i class="fa fa-check"></i>
                            </button></td>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->

                        </div>
                        <!-- /.panel-body -->

                    </div>
                    <!-- /.panel -->
                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">ADD</button>
                    <!--<button type="button" class="btn btn-primary btn-lg">DELETE</button>-->
                </div>
                <!-- /.col-lg-6 -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="Add Image" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content"> <form role="form" id="formpost" enctype="multipart/form-data" >
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Add Image</h4>
                                        </div>
                                        <div class="modal-body">
                                           
                                                <div class="form-group">
                                                    <label>Title</label>
                                                    <input class="form-control" name="txtTitle" >
                                                </div>
                                                <div class="form-group">
                                                    <label>File</label>
                                                    <input type="file"  name="txtFile" >
                                                </div>
                                            
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary"  >Save</button>
                                        </div>
                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>

<div class="modal fade" id="myEdit" tabindex="-1" role="dialog" aria-labelledby="Edit Image" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content"> <form role="form" id="formedit" enctype="multipart/form-data" >
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Edit Image</h4>
                                        </div>
                                        <div class="modal-body">
                                                <input type="hidden" value="" id="gallId" name="gallId" > 
                                                <div class="form-group">
                                                    <label>Title</label>
                                                    <input class="form-control" id="txtTitle" name="txtTitle" >
                                                </div>
                                                <div class="form-group">
                                                <img src="" id="imgeditPlace" >
                                                    <label>File</label>
                                                    <input type="file" id="txtFile2" name="txtFile2" >
                                                </div>
                                            
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" >Update</button>
                                        </div>
                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>                            

<div class="modal fade" id="myInfo" tabindex="-1" role="dialog" aria-labelledby="Image Info" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content"> <form role="form" id="formpost" enctype="multipart/form-data" >
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Image Info</h4>
                                        </div>
                                        <div class="modal-body">
                                                   
                                                <div class="form-group">
                                                    <img src="" id="imgplace" />
                                                    <p class="help-block" id="caption"></p>
                                                </div>
                                                <div class="form-group">
                                                    created <p class="help-block" id="created_date"></p>
                                                </div>
                                            
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="edit" class="btn btn-primary" id="editbtn" data-gallid=""  >Edit</button>
                                        </div>
                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>                            


            </div>